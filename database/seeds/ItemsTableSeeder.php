<?php

use App\Item;
use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = \Faker\Factory::create();

        for ($i=0; $i < 50 ; $i++) { 
            Item::create([
                    'title' => $faker->word,
                    'price' => $faker->numberBetween(10,1000),
                    'description' => $faker->text(200),
                    'image' => $faker->imageUrl(200, 200, 'cats')
            ]);
        }


    }
}
