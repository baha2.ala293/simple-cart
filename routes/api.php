<?php

Route::group(['prefix' => 'v1' , 'namespace' => 'Api\V1'], function() {
    
    Route::post('register'          , 'AuthController@register');
    Route::post('login'             , 'AuthController@login');
    Route::post('logout'            , 'AuthController@logout');
    Route::post('refresh/token'     , 'AuthController@refreshToken');

    Route::get('items'      , 'ItemsController@index');
    Route::get('items/{id}' , 'ItemsController@show');

    Route::post('basket/add/item'   , 'ShoppingBasketController@addItem');
    Route::get('basket'             , 'ShoppingBasketController@index');
    
});
