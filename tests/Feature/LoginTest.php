<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function testRequiresEmailAndPassword()
    {
        $this->json('POST', 'api/v1/login')
            ->assertStatus(422)
            ->assertJson(["error"=> [
                            "message"=> [
                                "email"=> ["The email field is required."],
                                "password"=> ["The password field is required."]
                            ],
        "status_code"=> 422
        ]
    ]);
    }

    public function testUserLoginWithWrongCredentials()
    {
        $user = factory(User::class)->create([
            'email' => 'test@gmail.com',
            'password' => bcrypt('123456'),
        ]);

        $payload = ['email' => 'test@gmail.com', 'password' => 'pppppp'];

        $this->json('POST', 'api/v1/login', $payload)
            ->assertStatus(401)
            ->assertJson(["error"=> [
                    "message"=> "Please check your credentials",
                    "status_code" => 401
                    ]
                ]);
    }

    public function testUserLoginSuccessfully()
    {
        $user = factory(User::class)->create([
            'email' => 'test@gmail.com',
            'password' => bcrypt('123456'),
        ]);

        $payload = ['email' => 'test@gmail.com', 'password' => '123456'];

        $this->json('POST', 'api/v1/login', $payload)
            ->assertStatus(200)
            ->assertJsonStructure(['token']);

    }
}
