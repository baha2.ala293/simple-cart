<?php

namespace Tests\Feature;

use App\Item;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use JWTAuth;
use Tests\TestCase;

class ShoppingBasketTest extends TestCase
{
    
    public function testAddItemToShoppingBasket()
    {
        $user = factory(User::class)->create();
        $token = JWTAuth::fromUser($user);
        $headers = ['Authorization' => "Bearer $token"];

        $item = factory(Item::class)->create();
        $payload = ['item_id' => $item->id];
        $this->json('POST', 'api/v1/basket/add/item', $payload, $headers)
            ->assertStatus(200)
            ->assertJson([ "success"=> [
                "message"=> "Item added to the Basket Successfully!",
                "status_code"=> 200
            ]]);
    }
}
