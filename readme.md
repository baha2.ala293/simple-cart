**Simple Shopping Basket**

## Installation
*  ### Server requirements
    - PHP >= 5.6.4
    - OpenSSL PHP Extension
    - PDO PHP Extension
    - Mbstring PHP Extension
    - Tokenizer PHP Extension
    - XML PHP Extension
    - 
*  [Composer Installed](https://getcomposer.org/download/)
* cd to your server directory and pull the project : https://gitlab.com/baha2.ala293/simple-cart.git
* cd to the project directory and run > composer update
* run > cp .example.env .env
* create an empty database and set the name of the database with credentials in the .env file
* run > chmod -R 777 storage
* run > php artisan key:generate
* run > php artisan jwt: generate
* run > php artisan config:cache 
* run > php artisan migrate
* run > php artisan db:seed


## Run tests
 > vendor/bin/phpunit