<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingBasketItem extends Model
{
    protected $table = "shopping_basket_items";

    protected $fillable = ['shopping_basket_id' , 'item_id'];

    public function items()
    {
        return $this->belongsToMany('App\Item','item_id');
    }
}
