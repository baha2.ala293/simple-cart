<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingBasket extends Model
{
    protected $table = 'shopping_basket';

    protected $fillable = ['user_id'];

    public function basket_items()
    {
        return $this->hasMany('App\ShoppingBasketItem','shopping_basket_id');
    }
}
