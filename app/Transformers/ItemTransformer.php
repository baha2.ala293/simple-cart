<?php

namespace App\Transformers;
use App\Transformers\BaseTransformer as Transformer;


class ItemTransformer extends Transformer
{

    public function transform( $item ) : array
    {
        return[
            'id'            => $item->id,
            'title'         => $item->title,
            'description'   => $item->description,
            'price'         => $item->price,
            'image'         => $item->image,
        ];
    }

}