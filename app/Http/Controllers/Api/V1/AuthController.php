<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseController as Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class AuthController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('jwt.auth')->only('logout');
    }

    public function register( Request $request )
    {
        $validator = Validator::make( $request->all(), [
            'name'          => 'required|string',
            'email'         => 'required|string|unique:users',
            'password'      => 'required|string|min:6|max:20',
        ]);
        
        if ($validator->fails()) {
            return $this->setStatusCode(422)->respondWithError($validator->messages());
        }

        $user = new User();
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return $this->respondCreated('Registered Successfully!');

    }


    public function login( Request $request )
    {
        $validator = Validator::make( $request->all(), [
            'email'         => 'required|string',
            'password'      => 'required|string|min:6|max:20',
        ]);
        
        if ($validator->fails()) {
            return $this->setStatusCode(422)->respondWithError($validator->messages());
        }

        try {
            if ( !$token = JWTAuth::attempt($request->all()) ) {
                return $this->respondUnauthorized('Please check your credentials');
            }
        } catch (JWTException $e) {
            return $this->setStatusCode(500)->respondWithError('can not create token');
        }

        return  $this->respond(['token'=> $token]);

    }


    public function refreshToken(Request $request)
    {
        $current_token =  JWTAuth::getToken();
        if ( !$current_token ) {
            return $this->setStatusCode(422)->respondWithError('can\'t get token');
        }
        try {
            $token = JWTAuth::refresh($current_token);
        } catch (JWTException $e) {
            return $this->respondForbidden();
        }
        return $this->respond(['token' =>$token]);
    }



    public function logout(Request $request)
    {
        $current_token  = JWTAuth::getToken();
        if ( !$current_token ) {
            return $this->setStatusCode(422)->respondWithError('can\'t get token');
        }
        JWTAuth::setToken( $current_token )->invalidate();
        return $this->respondWithSuccess('Logged out!');

    }






}
