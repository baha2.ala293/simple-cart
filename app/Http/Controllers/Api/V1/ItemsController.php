<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseController as Controller;
use App\Item;
use App\Transformers\ItemTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ItemsController extends Controller
{
    protected $itemTransformer;

    function __construct(ItemTransformer $itemTransformer)
    {
        $this->itemTransformer = $itemTransformer;
    }


    public function index( Request $request )
    {
        if ($request->limit) {
            $this->setPaignation($request->limit);
        }
        $pagination = Item::paginate($this->getPagination());
        $items  = $this->itemTransformer->transformCollection(collect($pagination->items()));
        return $this->respondWithPagination($pagination,['data' => $items]);
    }

    public function show( $id )
    {
        $item = Item::find($id);
        return  $item ? $this->respond(['data' => $this->itemTransformer->transform($item)]) : $this->respondNotFound();
    }



}
