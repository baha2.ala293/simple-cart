<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\V1\BaseController as Controller;
use App\ShoppingBasket;
use App\ShoppingBasketItem;
use App\Transformers\ItemTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ShoppingBasketController extends Controller
{
    protected $itemTransformer;

    function __construct( ItemTransformer $itemTransformer )
    {
        $this->middleware('jwt.auth');
        $this->itemTransformer = $itemTransformer;
    }


    public function index( Request $request )
    {
        $user =  $this->getAuthenticatedUser();

        if ($request->limit) {
            $this->setPagination($request->limit);
        }

        if (!$this->isHasBasket($user->id)) {
            return $this->respondWithError('You have no items in the basket!');
        }

        $pagination = ShoppingBasket::where('user_id', $user->id)
                                ->join('shopping_basket_items','shopping_basket.id','shopping_basket_items.shopping_basket_id')
                                ->join('items','shopping_basket_items.item_id','items.id')
                                ->paginate( $this->getPagination());
        $basket = $this->itemTransformer->transformCollection(collect($pagination->items()));
        return $this->respondWithPagination($pagination,['date' => $basket]);
    }


    public function addItem( Request $request )
    {
        $user =  $this->getAuthenticatedUser();

        $validator = Validator::make( $request->all(), [
            'item_id'          => 'required|exists:items,id',
        ]);
        
        if ($validator->fails()) {
            return $this->setStatusCode(422)->respondWithError($validator->messages());
        }

        ShoppingBasketItem::firstOrCreate([
            'item_id' => $request->item_id , 
            'shopping_basket_id' =>  $this->getBasketId($user->id)
        ]);

        return $this->respondWithSuccess('Item added to the Basket Successfully!');

    }


    public function isHasBasket( $user_id ) : bool
    {
        return  ShoppingBasket::where('user_id',$user_id)->first() ? true : false;
    }

    public function getBasketId( $user_id )
    {
        if (!$this->isHasBasket($user_id)) {
            $basket = ShoppingBasket::create(['user_id' => $user_id]);
        }

        $basket = ShoppingBasket::where('user_id' ,$user_id )->first();
        return $basket->id;
    }



}
